#!/usr/bin/env bash

# Launches a mesos cluster
#
# Before deployment, must first update parameters.json with output values
# from https://github.com/Clever/cloudformation-zookeeper/
#
#   aws cloudformation describe-stacks --stack-name <zookeeper-stack-id>
#

id=$1

if [[ -z $1 ]]; then
    echo "usage: ./launch-zk.sh <stack_id>"
    exit 1
fi

s3cmd put mesos-master.json s3://mesos-cf/
s3cmd put mesos-slave.json s3://mesos-cf/

aws cloudformation create-stack \
    --template-body file://mesos.json \
    --stack-name mesos-$id \
    --capabilities CAPABILITY_IAM \
    --parameters file://parameters.json

